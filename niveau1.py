
from random import *
class Monde:
    def _init_(self):
        self.dimension=50
        self.duree=30
        self.carte=[[0 for i in range(50)]for j in range(50)]
        for i in range(dimension*dimension//2):#on prend la moitié des carrés et on y met de l'herbe
            self.carte[randint(0,dimension-1)][randint(0,dimension-1)]=randint(30,100)
        for j in range(50): #on met des carrés non herbus pour le reste
            for i in range(50):
                if self.carte[j][i]==0:
                    self.carte[j][i]=randint(0,30)
    def herbePousse(self):
        for i in range(50):
            for j in range(50):
                self.carte[i][j]+=1
    def herbeMangee(self,i,j):
        self.carte[i][j]=0
    def nbHerbe(self):
        cmpt=0
        for i in self.carte:
            for j in i:
                if j>=self.duree:
                    cmpt=cmpt+1
        return cmpt
    def getCoefCarte(self, i, j):
        return self.carte[i][j]
    
class Mouton:
    
    def __init__(self,position_x,position_y,gain_nourriture=4,taux_reproduction=4):
        
        self.x=position_x
        self.y=position_y
        self.energie=randint(gain_nourriture,gain_nourriture*2)
        self.gain=gain_nourriture
        self.taux_reproduction=taux_reproduction
    
    
    def variationEnergie(self,monde):
        if monde.getCoefCarte(self.x,self.y)>=monde.duree : #vérifie si c'est un carré herbu
            self.energie+=self.gain
            monde.herbeMangee(self.x,self.y) #le mouton a mangé l'herbe donc on met le coefficient du carré à 0.  
        else:  
            self.energie-=1
        
    def deplacement(self,monde):
        a=[self.x-1,self.x+1,self.x]
        b=[self.y-1,self.y+1,self.y]
        self.x=choice(a) #choisit une des 8 cases adjacentes en sachant qu'il peut ne pas bouger
        self.y=choice(b)
        self.x=(self.x+monde.dimension)%monde.dimension #formule pour un monde torique, si le mouton dépasse les cases il reste dans le monde
        self.y=(self.y+monde.dimension)%monde.dimension
        
    def place_mouton(self):
        return Mouton(self.x,self.y) #retourne un mouton sur la même case
    
class Simulation:
    
    def __init__(self):
        self.horloge=0
    def simMouton(self):
    

